Mina Dot Is
=========

This site was built using HTML5, Sass, jQuery and Hammer.

## Current Sections ##
* [here](http://mina.is/here) - personal site/portfolio  
* [rambling](http://mina.is/rambling) - blog  
* [speaking](http://mina.is/speaking) - list of speaking engagements/slide decks  
* [super single](http://mina.is/supersingle) - fun experiment in data visualization  

## Planned Sections ##
* collection of helpful snippets and reminders, mostly for myself  

## Nerdy Bits ##

**Stuff used**  
* [Sass](http://sass-lang.com)  
* [Compass](http://compass-style.org)    
* [Hammer](http://hammerformac.com)  
* [Maven Pro](http://www.google.com/fonts/specimen/Maven+Pro) for body copy  

**Credits**
* portraits - Marco Sierra, April Sierra and Stacey Moore

**What's With the Weird Commits?**  
I'm using a random commit generator based on WhatTheCommit.com. Because it's fun.

```
git commit -m "`curl -s http://whatthecommit.com/index.txt`"
```


### CHANGELOG ###
**v4.0**  
= 09.04.2013 - initial commit =  
= 01.10.2013 - v4 launched =  
