# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "css"
sass_dir = "scss"
images_dir = "img"
javascripts_dir = "js"

# The environment mode. Defaults to :production, can also be :development
environment = ":development"

# You can select your preferred output style here (can be overridden via the command line):
output_style = :expanded # :expanded  or :nested or :compact or :compressed

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

# To disable debugging comments that display the original location of your selectors. 
# True in :development; false in :production
line_comments = true

preferred_syntax = :scss